<?php
/*CustomContent.php
Custom Content bundle for Symfony template
Copyright (C) 2016,2017 Daniel Fredriksen
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

namespace CYINT\ComponentsPHP\Bundles\CustomContentBundle\Entity;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * CustomContent
 */
class CustomContent
{
    private $id;
    private $title;
    private $friendlyUrl;
    private $navigations;
    private $content;
    private $created;
    private $updated;
    private $published = false;
    private $image;
    protected $localeField;
    
    public function __construct($title, $friendlyUrl, $content, $published = false)
    {
        $this->setNavigations(new ArrayCollection());
        $this->setTitle($title);
        $this->setFriendlyUrl($friendlyUrl);
        $this->setContent($content);
        $this->setCreated(time());
        $this->setPublished($published);
        $this->setLocaleField('en');
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return CustomContent
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set friendlyUrl
     *
     * @param string $friendlyUrl
     *
     * @return CustomContent
     */
    public function setFriendlyUrl($friendlyUrl)
    {
        $this->friendlyUrl = $friendlyUrl;

        return $this;
    }

    /**
     * Get friendlyUrl
     *
     * @return string
     */
    public function getFriendlyUrl()
    {
        return $this->friendlyUrl;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return CustomContent
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set created
     *
     * @param integer $created
     *
     * @return CustomContent
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return int
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param integer $upated
     *
     * @return CustomContent
     */
    public function setUpdated($upated)
    {
        $this->updated = $upated;

        return $this;
    }

    /**
     * Get upated
     *
     * @return int
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set published
     *
     * @param boolean $published
     *
     * @return CustomContent
     */
    public function setPublished($upated)
    {
        $this->published = $upated;

        return $this;
    }

    /**
     * Get upated
     *
     * @return boolean
     */
    public function getPublished()
    {
        return $this->published;
    }

    public function setNavigations(ArrayCollection $navigations)
    {
        $this->navigations = $navigations;
        return $this;
    }


    public function getNavigations()
    {
        return $this->navigations;
    }

    public function addNavigation(LinkRelationship $Navigation)
    {
        $this->navigations->add($Navigation);
        return $this;
    }

    public function removeNavigation(LinkRelationship $Navigation)
    {
        if($this->navigations->contains($Navigation))
            $this->navigations->remove($Navigation);

        return $this;
    }

    public function setLocaleField($localeField)
    {
        $this->localeField = $localeField;
        return $this;
    }

    public function getLocaleField()
    {
        return $this->localeField;
    }

    public function toArray()
    {
        return [
            'id'=>$this->getId()
            ,'title'=>$this->getTitle()
            ,'friendlyUrl' =>$this->getFriendlyUrl()
            ,'content' => $this->getContent()
            ,'created' => $this->getCreated()
            ,'updated' => $this->getUpdated()
            ,'published' => $this->getPublished()
            ,'image' => $this->getImage()
            ,'localeField' => $this->getLocaleField()       
        ];
    }

}
