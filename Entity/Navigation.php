<?php
/*Navigation.php
Custom Content bundle for Symfony template
Copyright (C) 2016,2017 Daniel Fredriksen
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

namespace CYINT\ComponentsPHP\Bundles\CustomContentBundle\Entity;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * Navigation
 */
class Navigation
{
    /**
     * @var int
     */
    private $id;
    private $name;
    private $created;
    private $target;
    private $links;

    public function __construct($name)
    {
        $this->setName($name);
        $this->setLinks(new ArrayCollection);
        $this->setCreated(time());
    }

    public function getId()
    {
        return $this->id;        
    }
 

    /**
     * Set created
     *
     * @param integer $created
     *
     * @return Navigation
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return int
     */
    public function getCreated()
    {
        return $this->created;
    }

    public function setLinks(ArrayCollection $links)
    {
        $this->links = $links;
        return $this;
    }


    public function getLinks()
    {
        return $this->links;
    }

    public function addLink(LinkRelationship $Link)
    {
        $this->links->add($Link);
        return $this;
    }

    public function removeLink(LinkRelationship $Link)
    {
        if($this->links->contains($Link))
            $this->links->remove($Link);

        return $this;
    }


    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function toArray()
    { 
        return [
            'id' => $this->getId()
            ,'name' => $this->getName()
            ,'created' => $this->getCreated()       
        ];

        //get links not implemented
    }

    public function getTarget()
    {
        return $this->target;
    }

    public function setTarget($target)
    {
        $this->target = $target;
        return $this;
    }

}
