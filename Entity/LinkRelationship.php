<?php
/*LinkRelationship.php
Custom Content bundle for Symfony template
Copyright (C) 2016,2017 Daniel Fredriksen
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

namespace CYINT\ComponentsPHP\Bundles\CustomContentBundle\Entity;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * LinkRelationship
 */
class LinkRelationship
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $sortOrder = 1;

    /**
     * @var string
     */
    private $linkText;

    /**
     * @var bool
     */
    private $active;

    /**
     * @var int
     */
    private $created;

    private $target;

    private $Navigation;
    private $CustomContent;
    protected $localeField;


    public function __construct(Navigation $Navigation, CustomContent $CustomContent, $linkText, $sortOrder, $active, $target)
    {
        $this->setNavigation($Navigation);
        $this->setCustomContent($CustomContent);
        $this->setlinkText($linkText);
        $this->setSortOrder($sortOrder);
        $this->setActive($active);
        $this->setTarget($target);
        $this->setCreated(time());
        $this->setLocaleField('en');
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;        
    }

    /**
     * Set sortOrder
     *
     * @param integer $sortOrder
     *
     * @return LinkRelationship
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;

        return $this;
    }

    /**
     * Get sortOrder
     *
     * @return int
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * Set linkText
     *
     * @param string $linkText
     *
     * @return LinkRelationship
     */
    public function setLinkText($linkText)
    {
        $this->linkText = $linkText;

        return $this;
    }

    /**
     * Get linkText
     *
     * @return string
     */
    public function getLinkText()
    {
        return $this->linkText;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return LinkRelationship
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return bool
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set created
     *
     * @param integer $created
     *
     * @return LinkRelationship
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return int
     */
    public function getCreated()
    {
        return $this->created;
    }

  
    public function setCustomContent($CustomContent)
    {
        $this->CustomContent = $CustomContent;
        return $this;
    }

    public function getCustomContent()
    {
        return $this->CustomContent;
    }

    public function setNavigation($Navigation)
    {
        $this->Navigation = $Navigation;
        return $this;
    }

    public function getNavigation()
    {
        return $this->Navigation;
    }


    public function setTarget($target)
    {
        $this->target = $target;
        return $this;
    }

    public function getTarget()
    {
        return $this->target;
    }

    public function setLocaleField($localeField)
    {
        $this->localeField = $localeField;
        return $this;
    }

    public function getLocaleField()
    {
        return $this->localeField;
    }

    public function toArray()
    {
		return [
			'id' => $this->getId()
			,'sortOrder' => $this->getSortOrder()
			,'linkText' => $this->getLinkText()
			,'active' => $this->getActive()
			,'created' => $this->getCreated()
			,'target' => $this->getTarget()
			,'navigation_id' => empty($this->getNavigation()) ? null : $this->getNavigation()->toArray()
			,'CustomContent' => empty($this->getCustomContent()) ?  null : $this->getCustomContent()->toArray()
			,'localeField' => $this->getLocaleField()
		];

    }
}
