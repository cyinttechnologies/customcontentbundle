<?php
/*NavigationRepository.php
Custom Content bundle for Symfony template
Copyright (C) 2016,2017 Daniel Fredriksen
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

namespace CYINT\ComponentsPHP\Bundles\CustomContentBundle\Repository;

use CYINT\ComponentsPHP\Bundles\CustomContentBundle\Factory\NavigationFactory;

class NavigationRepository extends \Doctrine\ORM\EntityRepository
{
    public function findByFilter($filter = null)
    {
        $query = $this->createQueryBuilder('n');
        if(!empty($filter))
        {
            $query
                ->where('n.name like :filter')
                ->setParameter(':filter', "%$filter%");
        }

        return $query->getQuery()->getResult();
    }

    public function getFactory($Doctrine, $Container)
    {
        return new NavigationFactory($this, $Doctrine, $Doctrine->getManager());
    }

}
