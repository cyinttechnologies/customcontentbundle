<?php
/*NavigationFactory.php
Custom Content bundle for Symfony template
Copyright (C) 2016,2017 Daniel Fredriksen
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

namespace CYINT\ComponentsPHP\Bundles\CustomContentBundle\Factory;

use CYINT\ComponentsPHP\Bundles\CustomContentBundle\Classes\ParseData;
use CYINT\ComponentsPHP\Bundles\CustomContentBundle\Entity\Navigation;
use CYINT\ComponentsPHP\Factory\MasterFactory;

class NavigationFactory extends MasterFactory
{
    protected $fieldKeys = ['name','created','target','links'];
    protected $EntityType = 'CYINT\ComponentsPHP\Bundles\CustomContentBundle\Entity\Navigation';

    public function __construct($Repository, $Doctrine, $Manager)
    {
        $this->setDoctrine($Doctrine);
        //Case mut match that used in the getter and setter method as 'get'and 'set' wll be appended to the keys.
         $this->setFields([
            'Id' => $this->initializeField(
                'none', null, null, null
            )
            ,'Name' => $this->initializeField(
                'text', 'Name','','',['required'] 
            )
            ,'Target' => $this->initializeField(
                'select', 'Target', null, null, []
                , $this->getTargetOptions()
            )
        ]);

        parent::__construct($Repository, $Doctrine, $Manager);
    }
    
    public function entityConstruction(&$Navigation)
    {
        $name = $this->fields['Name']['value'];
        if(empty($name))
            throw new \Exception('Name is required.');

        $Navigation = new Navigation($name);
    }

    public function getExceptionMessage(\Exception $Ex = null)
    {    
        switch(get_class($Ex))
        {
            case 'Doctrine\DBAL\Exception\UniqueConstraintViolationException':
                return 'A navigation with this name already exists, and navigation\' name must be unique. Please enter a new name and try again.';
            break;

            default:
                return $Ex->getMessage();
            break; 
        }
    }

    private function getTargetOptions()
    {
        return [
            'selectOptions' => [
                'type' => 'static'
                ,'options' => [
                    'None' => null
                    ,'New Window/Tab' => '_blank'
                ]
            ]
        ];
    }    
}
