<?php
/*CustomContentFactory.php
Custom Content bundle for Symfony template
Copyright (C) 2016,2017 Daniel Fredriksen
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

namespace CYINT\ComponentsPHP\Bundles\CustomContentBundle\Factory;

use CYINT\ComponentsPHP\Bundles\CustomContentBundle\Entity\CustomContent;
use CYINT\ComponentsPHP\Factory\MasterFactory;

class CustomContentFactory extends MasterFactory
{

    protected $fieldKeys = ['title', 'content', 'published'];
    protected $EntityType = 'CYINT\ComponentsPHP\Bundles\CustomContentBundle\Entity\CustomContent';

    public function __construct($Repository, $Doctrine, $Manager)
    {
        //Case mut match that used in the getter and setter method as 'get'and 'set' wll be appended to the keys.
         $this->setFields([
            'Id' => $this->initializeField(
                'none', null, null, null
            )
            ,'Title' => $this->initializeField(
                'text', 'Title','','',['required'] 
            )
            ,'Content' => $this->initializeField(
                'richtext', 'Content', '', ''
            )
            ,'Published' => $this->initializeField(
                'checkbox', 'Published', false, false
            )
        ]);

        parent::__construct($Repository, $Doctrine, $Manager);
    }

    public function entityConstruction(&$CustomContent = null)
    {
        $friendly_url = $this->getFriendlyURL($this->fields['Title']['value']); 
        $CustomContent = new CustomContent(
            $this->fields['Title']['value']
            ,$friendly_url
            ,$this->fields['Content']['value']
            ,$this->fields['Published']['value']

        );        
    }

    protected function entityEditUnique(&$CustomContent)
    {
        if($CustomContent->getId() < 5 || $this->getLocale() != 'en')
            $friendly_url = $CustomContent->getFriendlyUrl();
        else
            $friendly_url = $this->getFriendlyURL($this->fields['Title']['value']); 

        $CustomContent->setFriendlyUrl($friendly_url);
    }

    public function prepareUniqueData()
    {   
        return $this->fields;
    }

    public function getSuccessMessage($create = true)
    {
        if($create)
            return "The custom page has been created successfully.";
        else
            return "The custom page has been updated successfully.";
    }

    public function getExceptionMessage(\Exception $Ex = null)
    {     
        switch(get_class($Ex))
        {
            case 'Doctrine\DBAL\Exception\UniqueConstraintViolationException':
                return 'A custom page with this title already exists, and custom page titles must be unique. Please enter a new title and try again.';
            break;

            default:
                return $Ex->getMessage();
            break; 
        }
    }
 
    private function getFriendlyURL($input, $delimiter = '-')
    {
        $newar = array();

        $input = preg_split('@\s+@', strtolower($input)); // Separate words by space(s).
        $input = preg_replace('@[^A-Za-z0-9\-]@', '', $input); // Drop all non-alphanumeric characters.

        // At this point, each $input element contains either alphanumeric characters with no spaces or nothing (empty string).

        return implode($delimiter, $input);
    }

}
