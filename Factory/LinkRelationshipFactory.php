<?php
/*LinkRelationshipFactory.php
Custom Content bundle for Symfony template
Copyright (C) 2016,2017 Daniel Fredriksen
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/


namespace CYINT\ComponentsPHP\Bundles\CustomContentBundle\Factory;

use CYINT\ComponentsPHP\Bundles\CustomContentBundle\Classes\ParseData;
use CYINT\ComponentsPHP\Bundles\CustomContentBundle\Entity\LinkRelationship;
use CYINT\ComponentsPHP\Factory\MasterFactory;

class LinkRelationshipFactory extends MasterFactory
{
    protected $fieldKeys = ['link_text','CustomContent','Navigation','target','sort_order','active'];
    protected $EntityType = 'CYINT\ComponentsPHP\Bundles\CustomContentBundle\Entity\LinkRelationship';

    public function __construct($Repository, $Doctrine, $Manager)
    {
        $this->setDoctrine($Doctrine);
        //Case mut match that used in the getter and setter method as 'get'and 'set' wll be appended to the keys.
         $this->setFields([
            'Id' => $this->initializeField(
                'none', null, null, null
            )
            ,'LinkText' => $this->initializeField(
                'text', 'Link Text','','',['required'] 
            )
            ,'CustomContent' => $this->initializeField(
                'select', 'Page', null, null, ['required']
                , $this->getPageOptions()
            )
            ,'Navigation' => $this->initializeField(
                'select', 'Navigation', null, null, ['required']
                , $this->getNavigationOptions()
            )
            ,'Target' => $this->initializeField(
                'select', 'Target', null, null, []
                , $this->getTargetOptions()
            )
            ,'SortOrder' => $this->initializeField(
                'number', 'Position', 1, 1
            )
            ,'Active' => $this->initializeField(
                'checkbox', 'Active', false, false
            )
        ]);

        parent::__construct($Repository, $Doctrine, $Manager);
    }

    public function entityConstruction(&$LinkRelationship)
    {

        $Navigation = null; 
        $Navigation = $this->getDoctrine()->getRepository('CYINTCustomContentBundle:Navigation')->find($this->fields['Navigation']['value']);
        if(empty($Navigation))
            throw new \Exception('Could not find navigation entity associated with this id', 500);

        $CustomContent = null; 
        $CustomContent = $this->getDoctrine()->getRepository('CYINTCustomContentBundle:CustomContent')->find($this->fields['CustomContent']['value']);
        if(empty($CustomContent))          
            throw new \Exception('Could not find page associated with this page id');

        $LinkRelationship = new LinkRelationship(
            $Navigation 
            ,$CustomContent
            ,$this->fields['LinkText']['value']
            ,$this->fields['SortOrder']['value']
            ,$this->fields['Active']['value']
            ,$this->fields['Target']['value']
        );             

    }

    public function getExceptionMessage(\Exception $Ex = null)
    {    
        switch(get_class($Ex))
        {
            case 'Doctrine\DBAL\Exception\UniqueConstraintViolationException':
                return 'A link with this link text already exists, and link link texts must be unique. Please enter a new link text and try again.';
            break;

            default:
                return $Ex->getMessage();
            break; 
        }
    }

    private function getNavigationOptions()
    {
        $navigations = $this->getDoctrine()->getRepository('CYINTCustomContentBundle:Navigation')->findAll();
        return [
            'selectOptions' => [
                'type' => 'entity'
                ,'options' => $navigations
                ,'repository' => 'CYINTCustomContentBundle:Navigation'
                ,'valueGetter' => 'getId'
                ,'labelGetter' => 'getName'
            ]
        ];
    }

    private function getPageOptions()
    {
        $pages = $this->getDoctrine()->getRepository('CYINTCustomContentBundle:CustomContent')->findBy(['published'=>true]);  
        return [
            'selectOptions' => [
                'type' => 'entity'
                ,'options' => $pages
                ,'repository' => 'CYINTCustomContentBundle:CustomContent'
                ,'valueGetter' => 'getId'
                ,'labelGetter' => 'getTitle'
            ]
        ];
    }

    private function getTargetOptions()
    {
        return [
            'selectOptions' => [
                'type' => 'static'
                ,'options' => [
                    'None' => null
                    ,'New Window/Tab' => '_blank'
                ]
            ]
        ];
    }    
}
